# Capítulo 1: Diferença entre Kernel e Sistema Operacional; definição de ambos.

Quando você liga seu computador, você certamente vê uma tela preta com várias linhas dizendo um monte de coisa que você não entende. Então provavelmente aparece um logo e em seguida o seu desktop. Você decide que quer ouvir um pouco de música. Então é só abrir um player e deixar alguma playlist rodando. Enquanto isso, você abre o seu navegador e acessa o fórum. De repente, uma chamada no skype está lhe convocando para entrar (o skype abriu sozinho assim que seu desktop apareceu). Tudo normal como sempre para um usuário comum.

Se você estiver em uma máquina com Windows e eu lhe perguntar qual SO você usa, obviamente que você vai dizer Windows e a sua versão. Porém, se você estiver com uma máquina rodando Linux, qual seria a resposta? Aliás, você sabe o que significa um SO?

Ao abrir um programa, o SO será responsável pelo gerenciamento de tudo o que o programa precisará: uso de memória, comunicação de entrada e saída (aqui inclui o seu teclado, mouse, fone de ouvido etc), processos paralelos (threads), uma interface para o usuário (as janelinhas e botões) etc etc etc. É o SO quem vai oferecer e lidar com tudo isso e ele precisa executar tais tarefas de modo seguro e sistemático. Caso contrário, você terá algo instável e não usável.

Mas... E a pergunta sobre o Linux? Bom, você pode dizer a distribuição que está usando e a sua versão, mas eu perguntei sobre uma máquina rodando apenas Linux. E é aqui onde entra a diferença entre o Kernel e um SO.

O kernel é a terceira coisa que será carregada ao ligar um computador. Antes vem a BIOS, depois um bootloader (eu vou falar disso depois) e então o kernel.

Quando seu computador recebe a vida divina da eletricidade, a primeira coisa que ele faz é executar a BIOS que está mantida numa memória flash na placa mãe. BIOS significa Basic Input Output System. A princípio, ela iniciará vários serviços no hardware, depois irá apenas procurar por entradas e saídas de dispositivos (discos de armazenamento, uns trequinhos aí a mais), reconhecê-los e "carregá-los" para uso. Então começa a caçada por discos (seja rígido, cd ou flash, algo que armazene memória) que contém algo informativo para executar. A BIOS tentará ler um por um em ordem, até achar algo que faça sentido e então executar.

Com um SO instalado no seu HD, necessariamente também existirá um bootloader. Isso é um programinha que irá carregar o kernel na memória por fazer algumas checagens etc. Um bootloader é a primeira coisa que a BIOS deverá reconhecer. Os quatro primeiros bytes na partição serão responsáveis por dar instruções à BIOS para executá-lo. O bootloader é então carregado e executado. Para o kernel ser lido, o bootloader vai jogar a primeira instrução do kernel para o CPU, que daí em diante o kernel inteiro será carregado na memória RAM e então executado. Na verdade, o bootloader e o kernel estão na mesma imagem, então quando o bootloader é carregado para a memória, todo o kernel vai junto.

Tá, tá, mas e esse tal de kernel? O que ele faz?

O kernel é apenas um software muito complexo. Assim que começa a rodar, o kernel faz várias checagens no hardware. Primeiro ele tenta mudar o modo da CPU para long mode (que permitirá executar instruções 64 bits), depois arruma algumas algumas coisas ali, outras aqui (como o GDT, uns trecos do PIC, cria a stack, ajusta as páginas da stack etc).

Agora é a hora da realização: o kernel é a parte mais importante de um sistema operacional. O kernel é o responsável por oferecer todos aqueles recursos que foi dito no começo do capítulo. É ele quem vai gerenciar a memória RAM, a fim de proporcionar um melhor uso e também assegurar que nada irá quebrar (como acesso indevido de memória, por exemplo um programa tentando sobrescrever o kernel ou outro programa, ou código que quer ser executado em lugar de somente leitura etc). Ele também controlará os drivers: seja seu teclado, seu mouse, seja lá o que for. Para tudo isso, o kernel necessita de muitos algoritmos complexos, pois é ele quem vai estar se comunicando com o hardware o tempo todo. Ele abstrai a parte física e oferece uma interface amistosa para o SO. O kernel é o coração do SO.

O sistema operacional, em contrapartida, não faz nada disso. Ele apenas irá usar os recursos que o kernel oferece para gerenciar o espaço do usuário. Então, o SO em si é o que permite o usuário a usar o computador. O SO oferecerá vários programas para interface do usuário (como o ambiente de trabalho, que são as janelas, botões, etc). Trará o player de música, trará algum meio fácil para o usuário instalar programas e rodá-los. Enfim, ele apenas vai deixar tudo muito simples para você.

O papel do SO é disfarçar o kernel. Quando você vai usar um computador, você não quer que o sistema entre na sua frente (que o atrapalhe). Você não tem que saber que o SO existe, mas sim apenas usá-lo ao seu favor.

Uma última ressalva aqui: o Linux é apenas um kernel. O Ubuntu, Mint, Redhat etc, são distribuições. Ou seja, eles usam o Linux como kernel e fazem o resto do sistema operacional.

No próximo capítulo, iremos falar sobre gerenciamento de memória e como o kernel consegue abstrair a memória física para melhorar o uso e desempenho.

---

### Glossário

+ SO - Sistema Operacional.
+ GDT - [Global Descriptor Table](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-segmentation.pdf).
+ PIC - [Programmable Interrupt Controller](http://wiki.osdev.org/Interrupts).

