# Capítulo 2: Manipulação de memória.

Uma das mais importantes funções de um kernel é abstrair o hardware. Ou seja, tirar toda a parte complexa que envolve ao mexer no hardware diretamente, oferecendo uma interface de melhor manuseio. Isso significa que o kernel se ocupa em comunicar-se com o hardware na parte complicada, para que então outros programas em outra camada (ou até mesmo para implementar coisas do próprio kernel) possam ser desenvolvidos sem muita preocupação com o hardware em si (ou pelo menos sem muita complexidade envolvendo o hardware). Afinal, quando um software comum é desenvolvido, o programador não deve se preocupar com coisas muito próximas ao hardware.

E a memória é uma das coisas mais complexas de lidar.

Existem muitas formas de se abstrair a memória. Cada uma tem a suas vantagens, nas quais facilitam a manipulação dela, porém também trazem algumas desvantagens (que podem até irem contra a vantagem inicial). Hoje, temos ótimas formas para facilitar e melhorar o uso da memória implementadas nos sistemas operacionais atuais. São quase que iguais, mas diferem em alguns aspectos.

Primeiro, vamos entender como funciona a memória RAM.

RAM (Random Access Memory, ou Memória de Acesso Aleatório) é um hardware composto por vários chips de memória de tamanhos iguais, formando um pente de memória. Sua função é oferecer memória suficiente para os programas que serão rodados no computador. Esses programas precisam de uma memória temporária quando estão em uso, de acesso rápido e dinâmico. Cada byte no pente é representado por um endereço, cujo qual é a posição de um byte na memória.

Entenda isso como uma fileira enorme de bytes (do tamanho da capacidade da sua memória). Cada posição é o endereço de um byte. Então se tivermos 128 bytes de memória, teremos 128 endereços diferentes. O endereço é só um número que representa o lugar/posição do byte na fila.

Os programas usam a memória RAM para guardarem informações (como textos, números etc). E a própria RAM é usada para carregar os programas que são abertos (sim, quando um programa é aberto, ele inteiro é jogado na RAM). Desta forma, o processador consegue ler as instruções e assim executá-lo. Isso é um papo pra outro capítulo. O problema aqui é que o software não precisa saber quais bytes da RAM ele vai usar. Ele apenas quer um pedaço lá e pronto. Se você explicitamente colocar um endereço lá para ser usado, isso pode quebrar algo.

Digo, imagine que dois programas estão rodando. Então um deles usa um pedaço de memória que inicia no endereço 0x200 até 0x204. De repente, o outro programa acaba usando o mesmo endereço (0x200 - 0x204). Um programa acessando a memória do outro não é algo legal. Isso vai acabar quebrando o funcionamento um do outro e levará a um comportamento indefinido. Na pior das hipóteses, um programa pode acabar sobrescrevendo instruções do kernel! Não tendo uma maneira de dizer que tal memória está ocupada, livre etc, causará um caos enorme. Gente, precisamos de uma maneira para evitar isso. Algo que mantenha os programas seguros sempre que rodarem.

E aqui, amigos, entramos num problema grande.

A primeira ideia a pensar é de criar uma lista, com todos os endereços, dizendo qual está ocupado ou livre. Isso já resolveria o nosso problema, porém não é tão simples assim. Se você criar uma lista contendo todos os endereços, você vai estar usando a memória inteira! Oras, se para cada endereço é um byte...

