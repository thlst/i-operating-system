# Eu, Sispema Operacional

Esta é uma série curta sobre o funcionamento de sistemas operacionais, explicado de maneira simples. A audiência foco é aquele que compreende os básicos de computadores. Ter um conhecimento básico em inglês será conveniente.

A série manterá um capítulo por postagem. Cada postagem elaborará em um assunto específico. Eu pretendo falar das partes mais interessantes, uma vez que o resto é muito complicado e vocês não conseguirão acompanhar.

